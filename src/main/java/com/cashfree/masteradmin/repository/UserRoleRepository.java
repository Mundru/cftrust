package com.cashfree.masteradmin.repository;


import com.cashfree.masteradmin.entities.jpa.Role;
import com.cashfree.masteradmin.entities.jpa.User;
import com.cashfree.masteradmin.entities.jpa.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    List<UserRole> findByUser(User user);
    Optional<UserRole> findByUserAndRole(User user, Role role);
}
