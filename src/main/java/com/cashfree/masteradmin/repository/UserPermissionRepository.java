package com.cashfree.masteradmin.repository;

import com.cashfree.masteradmin.entities.jpa.Permission;
import com.cashfree.masteradmin.entities.jpa.User;
import com.cashfree.masteradmin.entities.jpa.UserPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserPermissionRepository extends JpaRepository<UserPermission, Long> {
    List<UserPermission> findByUser(User user);
    Optional<UserPermission> findByUserAndPermission(User userId, Permission permission);
}
