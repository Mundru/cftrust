package com.cashfree.masteradmin.repository;

import com.cashfree.masteradmin.entities.jpa.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {}
