package com.cashfree.masteradmin.repository;

import com.cashfree.masteradmin.entities.jpa.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {}
