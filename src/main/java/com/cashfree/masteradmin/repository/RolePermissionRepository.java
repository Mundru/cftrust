package com.cashfree.masteradmin.repository;

import com.cashfree.masteradmin.entities.jpa.Permission;
import com.cashfree.masteradmin.entities.jpa.Role;
import com.cashfree.masteradmin.entities.jpa.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermission, Long> {
    public List<RolePermission> findByRoleId(Long roleId);
    public Optional<RolePermission> findByRoleIdAndPermissionId(Long roleId, Long permId);
}
