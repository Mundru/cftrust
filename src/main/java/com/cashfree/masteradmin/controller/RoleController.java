package com.cashfree.masteradmin.controller;

import com.cashfree.masteradmin.entities.jpa.Permission;
import com.cashfree.masteradmin.entities.jpa.Role;
import com.cashfree.masteradmin.entities.jpa.RolePermission;
import com.cashfree.masteradmin.models.Request.RolePermissionRequest;
import com.cashfree.masteradmin.models.Request.RoleRequest;
import com.cashfree.masteradmin.services.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.common.util.impl.Log;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/role")
public class RoleController {

  private RoleService roleService;

  public RoleController(RoleService roleService) {
    this.roleService = roleService;
  }

  @PostMapping
  public Role saveRole(@RequestBody RoleRequest roleReq) {
    return roleService.createRole(roleReq);
  }

  @GetMapping
  public List<Role> getAllRoles(@RequestParam("page") int pageId) {
    return roleService.getRolesByPage(pageId);
  }

  @GetMapping("{id}")
  public Role getRoleById(@PathVariable("id") long roleId) {
    return roleService.getRoleById(roleId);
  }

  @GetMapping("/{roleId}/permissions")
  public List<RolePermission> getPermissionsOfRole(@PathVariable("roleId") Long roleId) {
    return roleService.getPermissionsOfRole(roleId);
  }

  @PutMapping("{id}")
  public String updateRole(@PathVariable("id") long id, @RequestBody RoleRequest roleReq) {
    return roleService.updateRole(roleReq, id);
  }


  @PostMapping("/{roleId}/permission")
  public String addPermissionToRoles(
          @PathVariable("roleId") long roleId, @RequestBody RolePermissionRequest rolePermissionRequest) {
    return roleService.addPermissionToRole(roleId, rolePermissionRequest);
  }

  @PutMapping("{id}/permission")
  public String updateRolePermission(@PathVariable("id") long id, @RequestBody RolePermissionRequest updateRolePerm) {
    return roleService.updateRolePermission(id, updateRolePerm);
  }

}
