package com.cashfree.masteradmin.controller;

import com.cashfree.masteradmin.entities.jpa.Permission;
import com.cashfree.masteradmin.models.Request.PermissionRequest;
import com.cashfree.masteradmin.services.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/permission")
public class PermissionController {
  private PermissionService permissionService;

  public PermissionController(PermissionService permissionService) {
    this.permissionService = permissionService;
  }

  @PostMapping
  public Permission savePermission(@RequestBody PermissionRequest permission) {
    return permissionService.createPermission(permission);
  }

  @GetMapping
  public List<Permission> getPermissionsByPage(@RequestParam("page") int pageId) {
    return permissionService.getPermissionsByPage(pageId);
  }

  @GetMapping("{id}")
  public Permission getPermissionById(@PathVariable("id") long permissionId) {
    return permissionService.getPermissionById(permissionId);
  }

  @PutMapping("{id}")
  public String updatePermission(
      @PathVariable("id") long id, @RequestBody PermissionRequest permissionReq) {
    return permissionService.updatePermission(permissionReq, id);
  }

}
