package com.cashfree.masteradmin.controller;

import com.cashfree.masteradmin.entities.jpa.*;
import com.cashfree.masteradmin.models.Request.UserPermissionRequest;
import com.cashfree.masteradmin.models.Request.UserRoleRequest;
import com.cashfree.masteradmin.models.Request.UserRequest;
import com.cashfree.masteradmin.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/user")
public class UserController {

  private UserService userService;

  public UserController(UserService userService){
    this.userService = userService;
  }

  @PostMapping
  public User saveRole(@RequestBody UserRequest userRequest) {
    return userService.createUser(userRequest);
  }

  @GetMapping
  public List<User> getUsersByPage(@RequestParam("page") int pageId) {
    return userService.getUsersByPage(pageId);
  }

  @GetMapping("{id}")
  public User getUserById(@PathVariable("id") long userId) {
    return userService.getUserById(userId);
  }

  @GetMapping("{userId}/permissions")
  public List<UserPermission> getPermissionsOfUser(@PathVariable("userId") long userId) {
    return userService.getPermissionsOfUser(userId);
  }

  @GetMapping("/{userId}/roles")
  public List<UserRole> getRolesOfUser(@PathVariable("userId") long userId) {
    return userService.getRolesOfUser(userId);
  }


  @PutMapping("{id}")
  public String updateUser(@PathVariable("id") long id, @RequestBody UserRequest updateUser) {
    return userService.updateUser(updateUser, id);
  }


  @PutMapping("{id}/permission")
  public String updateUserPermission(@PathVariable("id") long id, @RequestBody UserPermissionRequest updateUserPermission) {
    return userService.updateUserPermission(id, updateUserPermission);
  }


  @PostMapping("/{userId}/permission")
  public String addPermissionToUser(
          @PathVariable("userId") long userId, @RequestBody UserPermissionRequest permReq) {
    return userService.addPermissionToUser(userId,permReq);
  }

  @PutMapping("{id}/role")
  public String updateUserRole(@PathVariable("id") long id, @RequestBody UserRoleRequest updateUserRole) {
    return userService.updateUserRole(id, updateUserRole);
  }


  @PostMapping("/{userId}/Role")
  public String addRoleToUser(
      @PathVariable("userId") long userId, @RequestBody UserRoleRequest roleReq) {
    return userService.addRoleToUser(userId, roleReq);
  }




}
