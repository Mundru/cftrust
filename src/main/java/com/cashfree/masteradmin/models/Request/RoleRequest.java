package com.cashfree.masteradmin.models.Request;

import lombok.Data;

@Data
public class RoleRequest {
    private String name;
    private String description;
    private int status;
    private long updatedBy;
}
