package com.cashfree.masteradmin.models.Request;

import lombok.Data;

import javax.persistence.Column;

@Data
public class RolePermissionRequest {
    private int permId;
    private int status;
    private long updatedBy;
}
