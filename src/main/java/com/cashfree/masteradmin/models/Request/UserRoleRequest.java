package com.cashfree.masteradmin.models.Request;

import lombok.Data;

@Data
public class UserRoleRequest {
    private long roleId;
    private long updatedBy;
    private int status;
}
