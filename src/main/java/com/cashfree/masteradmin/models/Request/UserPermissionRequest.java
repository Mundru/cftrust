package com.cashfree.masteradmin.models.Request;

import lombok.Data;

@Data
public class UserPermissionRequest {
    private long permId;
    private long updatedBy;
    private int status;
    private int scope;
}
