package com.cashfree.masteradmin.models.Request;

import lombok.Data;

@Data
public class PermissionRequest {
    private String name;
    private long appId;
    private String scope;
    private int status;
    private long updatedBy;
}
