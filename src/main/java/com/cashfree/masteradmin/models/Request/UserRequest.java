package com.cashfree.masteradmin.models.Request;

import lombok.Data;

@Data
public class UserRequest {

    private String name;
    private String email;
    private int status;
    private long updatedBy;

}
