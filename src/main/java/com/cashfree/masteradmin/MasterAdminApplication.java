package com.cashfree.masteradmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasterAdminApplication {

  public static void main(String[] args) {
    SpringApplication.run(MasterAdminApplication.class, args);
  }
}
