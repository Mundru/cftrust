package com.cashfree.masteradmin.entities.jpa;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "permission")
@Data
public class Permission {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "application_id")
  private long appId;

  @Column(name = "scope")
  private String scope;

  @Column(name = "status")
  private int status;


  @Column(name = "updated_by")
  private long updatedBy;

  @Column(name = "updated_at")
  @UpdateTimestamp
  private LocalDateTime updatedAt;

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;

    if (obj == null || getClass() != obj.getClass()) return false;

    Permission permission = (Permission) obj;
    return Objects.equals(name, permission.name) && Objects.equals(appId, permission.getAppId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, appId);
  }
}
