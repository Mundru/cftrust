package com.cashfree.masteradmin.entities.jpa;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "user_permission")
@Data
public class UserPermission {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "status")
  private int status;

  @Column(name = "scope")
  private int scope;

  @Column(name = "updated_by")
  private long updatedBy;

  @Column(name = "updated_at")
  @UpdateTimestamp
  private LocalDateTime updatedAt;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "permission_id")
  private Permission permission;



  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;

    if (obj == null || getClass() != obj.getClass()) return false;

    UserPermission up = (UserPermission) obj;
    return Objects.equals(permission.getId(), up.permission.getId())
        && Objects.equals(user.getId(), up.user.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(user.getId(), permission.getId());
  }
}
