package com.cashfree.masteradmin.entities.jpa;


import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "user_role")
@Data
public class UserRole {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "status")
  private int status;


  @Column(name = "updated_by")
  private long updatedBy;

  @Column(name = "updated_at")
  @UpdateTimestamp
  private LocalDateTime updatedAt;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "role_id")
  private Role role;

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;

    if (obj == null || getClass() != obj.getClass()) return false;

    UserRole ur = (UserRole) obj;
    return Objects.equals(role.getId(), ur.role.getId())
        && Objects.equals(user.getId(), ur.user.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(user.getId(), role.getId());
  }
}
