package com.cashfree.masteradmin.services;

import com.cashfree.masteradmin.entities.jpa.Permission;
import com.cashfree.masteradmin.models.Request.PermissionRequest;

import java.util.List;

public interface PermissionService {
  Permission createPermission(PermissionRequest permissionReq);

  List<Permission> getPermissionsByPage(int pageId);

  Permission getPermissionById(long id);

  String updatePermission(PermissionRequest permissionReq, long id);
}
