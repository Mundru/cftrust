package com.cashfree.masteradmin.services;

import com.cashfree.masteradmin.entities.jpa.*;
import com.cashfree.masteradmin.models.Request.UserPermissionRequest;
import com.cashfree.masteradmin.models.Request.UserRoleRequest;
import com.cashfree.masteradmin.models.Request.UserRequest;

import java.util.List;

public interface UserService {
  // Creation
  User createUser(UserRequest user);

  // Read
  List<User> getUsersByPage(int pageId);
  User getUserById(long id);
  List<UserPermission> getPermissionsOfUser(long userId);
  List<UserRole> getRolesOfUser(long userId);

  // Update(delete)
  String updateUser(UserRequest updateUser, long id);
  String updateUserPermission(long userId, UserPermissionRequest userPermReq);
  String addPermissionToUser(long userId, UserPermissionRequest permReq);
  String updateUserRole(long userId, UserRoleRequest userRoleReq);
  String addRoleToUser(long userId, UserRoleRequest roleReq);
}
