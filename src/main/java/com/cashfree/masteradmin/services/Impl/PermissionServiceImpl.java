package com.cashfree.masteradmin.services.Impl;

import com.cashfree.masteradmin.entities.jpa.Permission;
import com.cashfree.masteradmin.exceptions.ResourceNotFoundException;
import com.cashfree.masteradmin.models.Request.PermissionRequest;
import com.cashfree.masteradmin.repository.PermissionRepository;
import com.cashfree.masteradmin.services.PermissionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {
  private PermissionRepository permissionRepository;

  public PermissionServiceImpl(PermissionRepository permissionRepository) {
    this.permissionRepository = permissionRepository;
  }

  @Override
  public Permission createPermission(PermissionRequest permissionReq) {
    Permission permission = new Permission();
    permission.setName(permissionReq.getName());
    permission.setStatus(permissionReq.getStatus());
    permission.setUpdatedBy(permissionReq.getUpdatedBy());
    permission.setScope(permissionReq.getScope());
    permission.setAppId(permissionReq.getAppId());
    return permissionRepository.save(permission);
  }

  @Override
  public List<Permission> getPermissionsByPage(int pageId) {
    PageRequest pageRequest = PageRequest.of(pageId, 2);
    Page<Permission> pageContent = permissionRepository.findAll(pageRequest);
    return pageContent.getContent();
  }

  @Override
  public Permission getPermissionById(long id) {
    return permissionRepository
        .findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Permission", "Id", id));
  }

  @Override
  public String updatePermission(PermissionRequest permissionReq, long id) {

    // we need to check whether Permission with given id is exist in DB or not
    Permission existingPermission =
        permissionRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Permission", "Id", id));

    existingPermission.setName(permissionReq.getName());
    existingPermission.setAppId(permissionReq.getAppId());
    existingPermission.setScope(permissionReq.getScope());
    existingPermission.setStatus(permissionReq.getStatus());
    existingPermission.setUpdatedBy(permissionReq.getUpdatedBy());

    permissionRepository.save(existingPermission);
    return "Updated Successfully";
  }

}
