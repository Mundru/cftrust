package com.cashfree.masteradmin.services.Impl;

import com.cashfree.masteradmin.entities.jpa.*;
import com.cashfree.masteradmin.exceptions.ResourceNotFoundException;
import com.cashfree.masteradmin.models.Request.UserPermissionRequest;
import com.cashfree.masteradmin.models.Request.UserRoleRequest;
import com.cashfree.masteradmin.models.Request.UserRequest;
import com.cashfree.masteradmin.repository.*;
import com.cashfree.masteradmin.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
  private UserRepository userRepository;
  private PermissionRepository permissionRepository;
  private RoleRepository roleRepository;
  private UserPermissionRepository userPermissionRepository;
  private UserRoleRepository userRoleRepository;

  @Autowired
  public UserServiceImpl(
      UserRepository userRepo, RoleRepository roleRepo,
      PermissionRepository permissionRepo,UserPermissionRepository userPermissionRepository,
      UserRoleRepository userRoleRepository) {
    this.roleRepository = roleRepo;
    this.permissionRepository = permissionRepo;
    this.userRepository = userRepo;
    this.userPermissionRepository = userPermissionRepository;
    this.userRoleRepository = userRoleRepository;
  }

  private Role getRoleObject(long roleId){
    return roleRepository
            .findById(roleId)
            .orElseThrow(() -> new ResourceNotFoundException("Role", "Id", roleId));
  }

  private Permission getPermissionObject(long permId){
    return permissionRepository
            .findById(permId)
            .orElseThrow(() -> new ResourceNotFoundException("Permission", "Id", permId));
  }

  private User getUserObject(long userId){
    return  userRepository
            .findById(userId)
            .orElseThrow(() -> new ResourceNotFoundException("User", "Id", userId));
  }

  @Override
  public User createUser(UserRequest userRequest) {
    User user = new User();
    user.setEmail(userRequest.getEmail());
    user.setStatus(userRequest.getStatus());
    user.setUpdatedBy(userRequest.getUpdatedBy());
    return userRepository.save(user);
  }

  @Override
  public List<User> getUsersByPage(int pageId) {
    PageRequest pageRequest = PageRequest.of(pageId, 2);
    Page<User> pageContent = userRepository.findAll(pageRequest);
    return pageContent.getContent();
  }

  @Override
  public User getUserById(long userId) {
    return getUserObject(userId);
  }


  // this currently gives gives permissions that are not part of any role for user.
  @Override
  @Transactional
  public List<UserPermission> getPermissionsOfUser(long userId) {
    User user = getUserObject(userId);
    return userPermissionRepository.findByUser(user);
  }

  @Override
  @Transactional
  public List<UserRole> getRolesOfUser(long userId) {
    User user = getUserObject(userId);

    return userRoleRepository.findByUser(user);
  }

  @Override
  @Transactional
  public String updateUser(UserRequest updateUser, long userId) {
    User existingUser = getUserObject(userId);

    existingUser.setEmail(updateUser.getEmail());
    existingUser.setName(updateUser.getName());
    existingUser.setStatus(updateUser.getStatus());
    existingUser.setUpdatedBy(updateUser.getUpdatedBy());

    userRepository.save(existingUser);
    return "Successfully Updated";
  }

  @Override
  @Transactional
  public String updateUserPermission(long userId, UserPermissionRequest userPermReq) {
    long permId = userPermReq.getPermId();
    User user = getUserObject(userId);
    Permission permission = getPermissionObject(permId);
    UserPermission userPermission =
            userPermissionRepository
                    .findByUserAndPermission(user, permission)
                    .orElseThrow(()->new ResourceNotFoundException("User do not have the given permission"));
    userPermission.setStatus(userPermReq.getStatus());
    userPermission.setUpdatedBy(userPermReq.getUpdatedBy());
    userPermission.setScope(userPermReq.getScope());

    return "Successfully updated permission Of user";
  }

  @Override
  @Transactional
  public String addPermissionToUser(long userId, UserPermissionRequest userPermReq) {
    long permId = userPermReq.getPermId();
    User user = getUserObject(userId);
    Permission permission = getPermissionObject(permId);
    UserPermission userPermission = new UserPermission();
    userPermission.setPermission(permission);
    userPermission.setUser(user);
    userPermission.setUpdatedBy(userPermReq.getUpdatedBy());
    userPermission.setStatus(userPermReq.getStatus());

    userPermissionRepository.save(userPermission);
    return "Permission is successfully added to User";
  }

  @Override
  @Transactional
  public String updateUserRole(long userId, UserRoleRequest userRoleReq) {
    long roleId = userRoleReq.getRoleId();
    User user = getUserObject(userId);
    Role role = getRoleObject(roleId);
    UserRole userRole =
            userRoleRepository
                    .findByUserAndRole(user, role)
                    .orElseThrow(()->new ResourceNotFoundException("User do not have the given role"));
    userRole.setStatus(userRoleReq.getStatus());
    userRole.setUpdatedBy(userRoleReq.getUpdatedBy());

    return "Successfully updated role of user";
  }

  @Override
  @Transactional
  public String addRoleToUser(long userId, UserRoleRequest userRoleReq) {
    long roleId = userRoleReq.getRoleId();
    User user = getUserObject(userId);
    Role role = getRoleObject(roleId);
    UserRole userRole = new UserRole();
    userRole.setRole(role);
    userRole.setUser(user);
    userRole.setUpdatedBy(userRoleReq.getUpdatedBy());
    userRole.setStatus(userRoleReq.getStatus());
    userRoleRepository.save(userRole);
    return "Role is successfully added to user";
  }
}
