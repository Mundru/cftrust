package com.cashfree.masteradmin.services.Impl;

import com.cashfree.masteradmin.entities.jpa.*;
import com.cashfree.masteradmin.exceptions.ResourceNotFoundException;
import com.cashfree.masteradmin.models.Request.RolePermissionRequest;
import com.cashfree.masteradmin.models.Request.RoleRequest;
import com.cashfree.masteradmin.repository.PermissionRepository;
import com.cashfree.masteradmin.repository.RolePermissionRepository;
import com.cashfree.masteradmin.repository.RoleRepository;
import com.cashfree.masteradmin.services.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Slf4j
@Service
public class RoleServiceImpl implements RoleService {
  private RoleRepository roleRepository;
  private PermissionRepository permissionRepository;
  private RolePermissionRepository rolePermissionRepository;

  @Autowired
  public RoleServiceImpl(RoleRepository roleRepository, PermissionRepository permissionRepository,
                         RolePermissionRepository rolePermissionRepository) {
    this.roleRepository = roleRepository;
    this.permissionRepository = permissionRepository;
    this.rolePermissionRepository = rolePermissionRepository;
  }

  private Role getRoleObject(long roleId){
    return roleRepository
            .findById(roleId)
            .orElseThrow(() -> new ResourceNotFoundException("Role", "Id", roleId));
  }

  private Permission getPermissionObject(long permId){
    return permissionRepository
            .findById(permId)
            .orElseThrow(() -> new ResourceNotFoundException("Permission", "Id", permId));
  }


  @Override
  public Role createRole(RoleRequest roleReq) {
    Role role = new Role();
    role.setName(roleReq.getName());
    role.setDescription(roleReq.getDescription());
    role.setStatus(roleReq.getStatus());
    role.setUpdatedBy(roleReq.getUpdatedBy());

    return roleRepository.save(role);
  }


  @Override
  public List<Role> getRolesByPage(int pageId) {
    PageRequest pageRequest = PageRequest.of(pageId, 2);
    Page<Role> pageContent = roleRepository.findAll(pageRequest);
    return pageContent.getContent();
  }

  @Override
  public Role getRoleById(long roleId) {
    return getRoleObject(roleId);
  }

  // you might  want to change return type in future, It's fine for now.
  @Override
  public List<RolePermission> getPermissionsOfRole(Long roleId) {
    return rolePermissionRepository.findByRoleId(roleId);
  }

  @Override
  @Transactional
  public String  updateRole(RoleRequest roleReq, long roleId) {
    Role existingRole = getRoleObject(roleId);

    existingRole.setDescription(roleReq.getDescription());
    existingRole.setStatus(roleReq.getStatus());
    existingRole.setName(roleReq.getName());
    existingRole.setUpdatedBy(roleReq.getUpdatedBy());

    roleRepository.save(existingRole);
    return "Updated Successfully";
  }

  @Override
  @Transactional
  public String updateRolePermission(long roleId, RolePermissionRequest rolePermissionReq) {
    long permId = rolePermissionReq.getPermId();
//    Role role = getRoleObject(roleId);
//    Permission permission = getPermissionObject(permId);
    RolePermission rolePermission =
            rolePermissionRepository
                    .findByRoleIdAndPermissionId(roleId, permId)
                    .orElseThrow(()->new ResourceNotFoundException("Role do not have the given permission"));

    rolePermission.setStatus(rolePermissionReq.getStatus());
    rolePermission.setUpdatedBy(rolePermissionReq.getUpdatedBy());

    return "RolePermission is successfully updated";
  }

  @Override
  @Transactional
  public String addPermissionToRole(long roleId, RolePermissionRequest rolePermRequest) {
    long permId = rolePermRequest.getPermId();
    Role role = getRoleObject(roleId);
    Permission permission = getPermissionObject(permId);
    RolePermission rolePermission = new RolePermission();

    rolePermission.setPermission(permission);
    rolePermission.setRole(role);
    rolePermission.setUpdatedBy(rolePermRequest.getUpdatedBy());
    rolePermission.setStatus(rolePermRequest.getStatus());
    rolePermissionRepository.save(rolePermission);
    return "Permission is successfully added to role";
  }

}
