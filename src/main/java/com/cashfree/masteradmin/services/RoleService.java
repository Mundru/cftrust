package com.cashfree.masteradmin.services;


import com.cashfree.masteradmin.entities.jpa.Role;
import com.cashfree.masteradmin.entities.jpa.RolePermission;
import com.cashfree.masteradmin.models.Request.RolePermissionRequest;
import com.cashfree.masteradmin.models.Request.RoleRequest;

import java.util.List;

public interface RoleService {
  // Creation
  Role createRole(RoleRequest roleReq);

  // Read
  List<Role> getRolesByPage(int pageId);
  Role getRoleById(long id);
  List<RolePermission> getPermissionsOfRole(Long roleId);

  // Update(delete)
  String updateRole(RoleRequest roleRequest, long id);
  String updateRolePermission(long roleId, RolePermissionRequest rolePermissionRequest);
  String addPermissionToRole(long roleId, RolePermissionRequest rolePermissionRequest);
}
